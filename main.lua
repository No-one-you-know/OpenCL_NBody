local ffi = require 'ffi'
local templet = require 'templet'
local task = require 'task'
local bit = require 'bit'
local band, bxor, bor, bnot = bit.band, bit.boxr, bit.bor, bit.bnot
local brshift, blshift = bit.rshift, bit.lshift

local T, TBuffer
local states, buffers = {0, 0}, {0, 0}
local stateIndex = 1
local N, GN, steps = 1024, 0, 1
local MegaSize = 1
local G, amortization = 6.67384e-5, 0.128
local minMass, maxMass = 1e4, 1e8
local name = 'nbody'

local unprocessedArguments = 
{	'platformId=1', 
	'deviceId=1', 
	'workgroupSize=256', 
}

local args = task.prepareArguments( unprocessedArguments )
local env = {}

local paramList = { args = args, env = env }

local actualdx, actualdy, dx, dy = 0, 0, 0, 0
local actualScale, scale = 1, 1
local scaleFactor, minScaleFactor, maxScaleFactor = 2, 0.125, 16
local moveFactor = 10
local screenDimensions = {love.graphics.getDimensions()}
local spriteBatch
local canvas
local colors = {}

local running = false
local tmp, tmp2
local pressedKeys, keysCache = {}, {}
local keyHandlers = 
{
	['escape'] = function() love.event.push('quit') end,
	['space']  = function()
			running = not running
			pressedKeys['space'] = false
		end,
	
	['kp-'] = function()
			pressedKeys['kp-'] = false
			if actualScale <= minScaleFactor then return end
			
			actualdx = (actualdx + screenDimensions[1]/2) / scaleFactor
			actualdy = (actualdy + screenDimensions[2]/2) / scaleFactor
			
			actualScale = actualScale / scaleFactor
		end,
	['kp+'] = function()
			pressedKeys['kp+'] = false
			if actualScale >= maxScaleFactor then return end
			
			actualScale = actualScale * scaleFactor
			
			actualdx = actualdx * scaleFactor - screenDimensions[1]/2
			actualdy = actualdy * scaleFactor - screenDimensions[2]/2
		end,
	
	['up'] = function() actualdy = actualdy + moveFactor end,
	['down'] = function() actualdy = actualdy - moveFactor end,
	['left'] = function() actualdx = actualdx + moveFactor end,
	['right'] = function() actualdx = actualdx - moveFactor end,
}

function love.load()
	local particle = love.graphics.newImage('resources/particle.png')
	spriteBatch = love.graphics.newSpriteBatch( particle, N+1 )
	canvas = love.graphics.newCanvas()
	
	for k, v in pairs(keyHandlers) do
		table.insert( keysCache, k )
	end
	
	for i = 1, N do
		colors[i] = {love.math.random(256),love.math.random(256),love.math.random(256)}
	end
	
	env.platform = task.getPlatform(paramList)
	env.device = task.getDevice(paramList)
	
	env.context = task.createContext(paramList)
	env.queue = task.createCommandQueue(paramList)
	env.workgroupSize = task.getWorkgroupSize(paramList)
	env.numberWorkgroups = task.getNumberWorkgroups(paramList)
	env.globalSize = task.getGlobalSize(paramList)
	
	env.numberWorkgroups = math.ceil(N / env.workgroupSize)
	
	print( string.format("Using platform: %s", env.platform.get_info(env.platform, "name")) )
	print( string.format("Using device: %s", env.device.get_info(env.device, "name")) )
	print( string.format("Using workgroup size = %d", env.workgroupSize) )
	print( string.format("Using number of workgroups size = %d", env.numberWorkgroups) )
	print( string.format("Using #workitems = %d", env.globalSize) )
	
	local file = io.open( name..'.templet' )
	local source = file:read("*a")
	file:close()
	local temp = templet.loadstring(source)
	
	source = temp{
		['N'] = N,
		['G'] = G,
		['workgroupSize'] = env.workgroupSize,
		['amort'] = amortization
	}
	
	file = io.open(name..'.cl', 'w')
	file:write( source )
	file:close()
	
	env.program = env.context:create_program_with_source( source )
	assert(env.program)
	
	local status, err = pcall( function() return env.program.build(env.program) end )
	io.stderr.write(io.stderr, env.program.get_build_info(env.program, env.device, "log"))
	if not status then    error(err)    end
	
	env.kernel = env.program:create_kernel( name )
	assert(env.kernel)
	
	T = env.context:create_buffer( "read_only", ffi.sizeof('cl_float') )
	for i = 1, #states do
		states[i] = env.context:create_buffer( "read_write", N * ffi.sizeof('cl_float8') )
	end
	
	TBuffer = ffi.cast('cl_float *', env.queue:enqueue_map_buffer(T, true, "write"))
	for i = 1, #states do
		buffers[i] = ffi.cast('cl_float8 *', env.queue:enqueue_map_buffer(states[i], true, "write"))
	end
	
	TBuffer[0] = 0
	
	for i = 0, N-1 do
		
		buffers[stateIndex][i].x = love.math.random()*screenDimensions[1]
		buffers[stateIndex][i].y = love.math.random()*screenDimensions[2]
		buffers[stateIndex][i].z = 0 --love.math.random()*screenDimensions[1]
		
		buffers[stateIndex][i].s3 = 0
		buffers[stateIndex][i].s4 = 0
		buffers[stateIndex][i].s5 = 0
		buffers[stateIndex][i].s6 = 0
		buffers[stateIndex][i].s7 = love.math.random(minMass, maxMass)
		
		buffers[stateIndex % #states + 1][i].s7 = buffers[stateIndex][i].s7
	end
	
	for i = 1, GN do
		buffers[stateIndex][i-1].s7 = maxMass*MegaSize  -- Mega Stars
	end
	
	env.queue:enqueue_unmap_mem_object( T, TBuffer )
	for i = 1, #states do
		env.queue:enqueue_unmap_mem_object( states[i], buffers[i] )
	end
	
	for i = 1, #states do
		buffers[i] = ffi.cast('cl_float8 *', env.queue:enqueue_map_buffer(states[i], true, "read"))
	end
	
	tmp = {env.globalSize}
	tmp2 = {env.workgroupSize}
	
end

function love.update(dt)
	scale = scale + (actualScale - scale)*dt
	dx = dx + (actualdx - dx)*dt
	dy = dy + (actualdy - dy)*dt
	
	for i = 1, #keysCache do
		if pressedKeys[ keysCache[i] ] then
			keyHandlers[keysCache[i]]()
		end
	end
	
	if not running then return end
	
	TBuffer = ffi.cast('cl_float *', env.queue:enqueue_map_buffer(T, true, "write"))
	TBuffer[0] = dt
	env.queue:enqueue_unmap_mem_object( T, TBuffer )
	
	for i = 1, #states do
		env.queue:enqueue_unmap_mem_object( states[i], buffers[i] )
	end
	
	for i = 1, steps do
		env.kernel:set_arg(0, states[stateIndex] )
		env.kernel:set_arg(1, states[stateIndex % #states +1] )
		env.kernel:set_arg(2, T )
		
		env.queue:enqueue_ndrange_kernel(
			env.kernel, nil, tmp, tmp2
		)
		
		stateIndex = stateIndex % #states +1
	end
	
	for i = 1, #states do
		buffers[i] = ffi.cast('cl_float8 *', env.queue:enqueue_map_buffer(states[i], true, "read"))
	end
	
end

function love.draw()
	
	--if running then
		--print( string.format("FPS: %d", love.timer.getFPS()) )
	--end
	
	love.graphics.translate( dx, dy )
	love.graphics.scale( scale )
	
	
	for i = 0, N-1 do
		if buffers[stateIndex][i].s3 >= 0 then
			love.graphics.setColor( colors[i+1] )
			love.graphics.circle('fill', buffers[stateIndex][i].x, buffers[stateIndex][i].y, 1)
		end
	end
end


function love.keypressed(key)
	pressedKeys[key] = true
end
function love.keyreleased(key)
	pressedKeys[key] = false
end


function love.quit()
	env.queue:enqueue_unmap_mem_object( T, TBuffer )
	for i = 1, #states do
		env.queue:enqueue_unmap_mem_object( states[i], buffers[i] )
	end
	TBuffer = nil
	states = nil
	buffers = nil
	
	env.queue:flush()
	env.queue:finish()
end
