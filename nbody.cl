#define NUM_ITEMS 1024
#define GRAVITY_CONST 6.67384e-05f
#define WORKGROUPSIZE 256
#define AMORTIZATION 0.128f

__kernel void nbody( __global const float8 * P, __global float8 * NP, __global const float * T ){
  // Position = P[i].(x, y, z)
  // Velocity = P[i].(s4, s5, s6)
  // Mass = P[i].s7
  // P is the actual state of the bodies
  // NP is the next state being computed
  
  const long gid = get_global_id(0);
  if(gid >= NUM_ITEMS) return;
  
  NP[gid].s3 = P[gid].s3;  // Set the flag of the previous state to the next one
  if(P[gid].s3 < 0) {return;}
  
  float3 A, F, R;
  float d, f;
  int i, j;
  
	A.x = F.x = R.x = 0;
	A.y = F.y = R.y = 0;
  
  NP[gid].s4 = P[gid].s4;
  NP[gid].s5 = P[gid].s5;
  
  for(i = 0; i < NUM_ITEMS; i++){
		if(i == gid) {continue;}
		if(P[i].s3 < 0) {continue;}
		
		R.x = P[i].x - P[gid].x;
		R.y = P[i].y - P[gid].y;
		
		d = sqrt( R.x*R.x + R.y*R.y ) + AMORTIZATION;
		
		f = (GRAVITY_CONST * P[gid].s7 * P[i].s7) / (d*d);
		
		if (d < 4){
			if( P[gid].s7 > P[i].s7 ){
				NP[gid].s7 = P[gid].s7 + P[i].s7;
				NP[gid].s4 = ((P[gid].s7-P[i].s7)*P[gid].s4 + (2*P[i].s7)*P[i].s4)/(P[gid].s7+P[i].s7);
				NP[gid].s5 = ((P[gid].s7-P[i].s7)*P[gid].s5 + (2*P[i].s7)*P[i].s5)/(P[gid].s7+P[i].s7);
				f = 0;
			} else{
				NP[gid].s3 = -1;
				return;
			}
		}
		
		F.x += (f * R.x/ d);
		F.y += (f * R.y/ d);
  }
  
  A.x = F.x / P[gid].s7;
  A.y = F.y / P[gid].s7;
  
  NP[gid].s3 = 0;
  NP[gid].s7 = P[gid].s7;

  NP[gid].s4 += A.x * T[0];
  NP[gid].s5 += A.y * T[0];
  
  NP[gid].x = P[gid].x + NP[gid].s4*T[0] + 0.5f*A.x * T[0]*T[0];
  NP[gid].y = P[gid].y + NP[gid].s5*T[0] + 0.5f*A.y * T[0]*T[0];
  
  return;
}