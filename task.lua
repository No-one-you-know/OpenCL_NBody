local task = {}

local opencl  = require 'opencl'

function task.getArg(arg, t, defaultValue)
	if t == "s" then  -- string
		return string.format("%s", arg or defaultValue)
	end
	if t == "n" then  -- number
		return tonumber( string.format("%d", arg or defaultValue) )
	end
	if t == "f" then  -- float
		return tonumber( string.format("%.4f", arg or defaultValue) )
	end
	if t == "b" then  -- boolean
		if arg == nil then
			return defaultValue
		end
		
		if arg == false then
			return false
		else
			return true
		end
	end
end

function task.prepareArguments( args )
	if not args then    return {}   end
	
	local regex, parsedArgs = "^([^=]+)=?(.*)$", {}
	local key, value

	for i = 1, #args do
		key, value = args[i]:match( regex )
		assert(key, string.format("Invalid argument: %s", args[i]))
		parsedArgs[key] = value or true
	end
	
	return parsedArgs
end

function task.getPlatform( t )
	local args = t.args
	local env = t.env
	local platforms, platformId

	platforms = assert(opencl.get_platforms(), "Error getting platforms.")
	assert(#platforms >= 1, "There are no Opencl platforms.")
	
	platformId = task.getArg(args.platformId, "n", 1)
	assert(platformId >= 1 and platformId <= #platforms, 
		"Invalid platformId passed."
	)
	
	return platforms[ platformId ]
end

function task.getDevice( t )
	local args = t.args
	local env  = t.env
	local platform = env.platform
	local devices, deviceId
	
	assert(platform, "No valid platform passed.")
	
	devices = assert(platform:get_devices(), "Error getting devices.")
	assert(#devices >= 1, "There are no devices belonging to this platform." )
	
	deviceId = task.getArg( args.deviceId, "n", 1 )
	assert(deviceId >= 1 and deviceId <= #devices, "Invalid deviceId passed.")
	
	return devices[ deviceId ]
end

function task.createContext( t )
	local args = t.args
	local env = t.env
	local device = env.device
	
	assert(device, "No valid device passed.")
	
	return assert(opencl.create_context{device}, "Error creating device context.")
end

function task.createCommandQueue( t )
	local args = t.args
	local env = t.env
	local context = env.context
	local device = env.device
	
	assert(device, "No valid device passed.")
	assert(context, "No valid device context passed.")
	
	return context.create_command_queue( context, device )
end

function task.getWorkgroupSize( t )
	local args = t.args
	local env = t.env
	local device = env.device
	
	assert(device, "No valid device passed.")
	
	--local workgroupSizeDenominator = device.get_info(device, "")
	--local minWorkgroupSize = device.get_info(device, "min_work_group_size")
	local maxWorkgroupSize = device.get_info(device, "max_work_group_size")
	local workgroupSize = task.getArg(args.workgroupSize, 'n', 64)
	
	--assert(workgroupSize >= minWorkgroupSize and workgroupSize <= maxWorkgroupSize,
		--"Invalid workgroup size passed"
	--)
	
	return workgroupSize
end

function task.getNumberWorkgroups( t )
	local args = t.args
	local env = t.env
	
	local numberWorkgroups = task.getArg(args.numberWorkgroups, 'n', 64)
	
	assert(numberWorkgroups >= 1, "Invalid number of workgroups passed.")
	
	return numberWorkgroups
end

function task.getGlobalSize( t )
	local args = t.args
	local env = t.env
	local workgroupSize = env.workgroupSize
	local numberWorkgroups = env.numberWorkgroups
	
	assert(workgroupSize, "No workgroup size passed." )
	assert(numberWorkgroups, "No number of workgroups passed." )
	
	local globalSize = workgroupSize * numberWorkgroups
	
	assert(globalSize > 0, "Invalid globalSize, check workgroups dimensions.")
	
	return globalSize
end

return task